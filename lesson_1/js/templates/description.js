import {getBookById} from "../stores/bookStore.js";


export function showDescription(id) {
    let book = getBookById(id);
    console.log(book);

    let descBook = document.createElement('div');
    let table = document.querySelector('table');
    table.replaceWith(descBook);
    createProp(book, descBook);

    let backButton = document.createElement('button');
    backButton.textContent = 'Назад';
    backButton.onclick = ()=> {
        descBook.replaceWith(table);
    };

    descBook.appendChild(backButton);

}

function createProp(book, descBook) {
    for (let key in book) {
        let p = document.createElement('p');
        p.textContent = book[key];
        descBook.append(p);
    }
}