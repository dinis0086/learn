import {getBooks, getTableHead} from './stores/bookStore.js'
import {showDescription} from "./templates/description.js";


function createTable() {
    let table = document.createElement('table');
    return table;
}

function createTableHead(table, data) {
    let thead = table.createTHead();
    let row = thead.insertRow();
    for (let key of data) {
        let th = document.createElement('th');
        let text = document.createTextNode(key);
        th.appendChild(text);
        row.appendChild(th);
    }
}

function rowOnclick(id) {
    showDescription(id)
}

function createTableBody(table, data) {
    let tbody = table.createTBody();
    data.forEach((book) => {
        let row = tbody.insertRow();
        row.onclick = () => rowOnclick(book.id);
        for (let key in book) {
            let cell = row.insertCell();
            let text = document.createTextNode(book[key]);
            cell.append(text);
        }
    })
}

function main() {
    let mainDiv = document.getElementById('main');
    let table = createTable();
    mainDiv.appendChild(table);
    createTableHead(table, getTableHead());
    createTableBody(table, getBooks());
}

main();