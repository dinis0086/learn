let books = [
    {id: 1, author: 'Пушкин А. С.', title: 'Руслан и Людмилла', year: 1786, description: 'Мы не помним когда это было написано'},
    {id: 2, author: 'Лермонтов', title: 'Стихи', year: 1786, description: 'Мы не помним когда это было написано'},
    {id: 3, author: 'Чехов', title: 'Палата №6', year: 1786, description: 'Мы не помним когда это было написано'},
    {id: 4, author: 'Достоевский', title: 'Идиот', year: 1786, description: 'Мы не помним когда это было написано'},
];

let tableHead = ['id', 'Автор', 'Название', 'Год', 'Описание'];

export function getBooks() {
    return books;
}

export function getTableHead() {
    return tableHead;
}

export function getBookById(id) {
    return books.filter(item => item.id === id)[0];
}